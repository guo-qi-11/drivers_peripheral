# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

hdf_audio_path = "./../../../.."
hdf_audio_test_path = "./../../.."

if (defined(ohos_lite)) {
  import("//build/lite/config/component/lite_component.gni")
  import("//build/lite/config/test.gni")
  import("$hdf_audio_path/audio.gni")
} else {
  import("//build/ohos.gni")
  import("//build/test.gni")
  import("$hdf_audio_path/audio.gni")
}

if (defined(ohos_lite)) {
  unittest("hdf_audio_hdi_headset_test") {
    sources = [ "./src/analog_headset_test.cpp" ]

    include_dirs = [
      "include",
      "$hdf_audio_test_path/input/hal/include",
      "$hdf_audio_test_path/input/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "//third_party/FreeBSD/sys/dev/evdev",
    ]

    public_deps = [
      "$hdf_audio_path/hal/hdi_passthrough:hdi_audio",
      "$hdf_audio_test_path/input/hal:hdi_input",
      "//drivers/hdf_core/adapter/uhdf/manager:hdf_core",
      "//drivers/hdf_core/adapter/uhdf/platform:hdf_platform",
      "//drivers/hdf_core/adapter/uhdf/posix:hdf_posix_osal",
      "//drivers/hdf_core/adapter/uhdf/test/unittest/common:hdf_test_common",
      "//third_party/bounds_checking_function:libsec_shared",
    ]

    cflags = [
      "-Wall",
      "-Wextra",
      "-Werror",
      "-fsigned-char",
      "-fno-common",
      "-fno-strict-aliasing",
      "-std=c++11",
    ]
    if (ohos_build_compiler != "clang") {
      cflags += [
        "-Wno-format",
        "-Wno-format-extra-args",
      ]
    }
  }
} else {
  module_output_path = "hdf/audio/hal"
  ohos_unittest("hdf_audio_hdi_headset_test") {
    module_out_path = module_output_path
    sources = [ "./src/analog_headset_test.cpp" ]

    include_dirs = [ "include" ]

    deps = [
      "$hdf_audio_path/hal/hdi_passthrough:hdi_audio_common",
      "$hdf_audio_test_path/input/hal:hdi_input",
      "//drivers/hdf_core/adapter/build/test_common:libhdf_test_common",
    ]
    if (!drivers_peripheral_audio_feature_user_mode) {
      deps += [ "$hdf_audio_path/hal/hdi_passthrough:hdi_audio" ]
    }

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "hdf_core:libhdf_utils",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [
        "hdf_core:libhdf_utils",
        "hilog:libhilog",
      ]
    }
    cflags = [
      "-Wall",
      "-Wextra",
      "-Werror",
      "-fsigned-char",
      "-fno-common",
      "-fno-strict-aliasing",
    ]
  }
}
