# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

hdf_audio_path = "./../.."
hdf_hdi_service_path = "./.."
import("//build/ohos.gni")
import("$hdf_audio_path/audio.gni")

if (defined(ohos_lite)) {
  ohos_shared_library("audio_capture_adapter") {
    sources = [
      "$hdf_audio_path/supportlibs/adm_adapter/src/audio_interface_lib_capture.c",
      "$hdf_audio_path/supportlibs/adm_adapter/src/audio_interface_lib_common.c",
      "$hdf_hdi_service_path/primary_impl/src/audio_common.c",
    ]

    include_dirs = [
      "$hdf_hdi_service_path/primary_impl/include",
      "$hdf_audio_path/supportlibs/adm_adapter/include",
      "$hdf_audio_path/supportlibs/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "//base/hiviewdfx/hilog_lite/interfaces/native/kits/",
      "$hdf_hdi_service_path/vendor_interface/utils",
    ]

    public_deps = [ "drivers_interface_audio:audio_idl_headers" ]
    deps = [ "//third_party/bounds_checking_function:libsec_shared" ]
    external_deps = [
      "hdf_core:libhdf_utils",
      "hilog_featured_lite:hilog_shared",
    ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_shared_library("audio_render_adapter") {
    sources = [
      "$hdf_audio_path/supportlibs/adm_adapter/src/audio_interface_lib_common.c",
      "$hdf_audio_path/supportlibs/adm_adapter/src/audio_interface_lib_render.c",
      "$hdf_hdi_service_path/primary_impl/src/audio_common.c",
    ]

    public_deps = [ "drivers_interface_audio:audio_idl_headers" ]

    include_dirs = [
      "$hdf_hdi_service_path/primary_impl/include",
      "$hdf_audio_path/supportlibs/adm_adapter/include",
      "$hdf_audio_path/supportlibs/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "//base/hiviewdfx/hilog_lite/interfaces/native/kits/",
      "$hdf_hdi_service_path/vendor_interface/utils",
    ]

    deps = [ "//third_party/bounds_checking_function:libsec_shared" ]

    external_deps = [ "hilog_featured_lite:hilog_shared" ]
    if (enable_audio_hal_hdf_log) {
      defines = [ "AUDIO_HDF_LOG" ]
    }
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }
} else {
  ohos_shared_library("audio_capture_adapter") {
    sources = [ "$hdf_hdi_service_path/primary_impl/src/audio_common.c" ]
    if (drivers_peripheral_audio_feature_alsa_lib) {
      sources += [
        "$hdf_audio_path/supportlibs/alsa_adapter/src/alsa_lib_capture.c",
        "$hdf_audio_path/supportlibs/alsa_adapter/src/alsa_lib_common.c",
        "$hdf_audio_path/supportlibs/alsa_adapter/src/alsa_mixer_path.c",
        "//third_party/cJSON/cJSON.c",
      ]
    } else {
      sources += [
        "$hdf_audio_path/supportlibs/adm_adapter/src/audio_interface_lib_capture.c",
        "$hdf_audio_path/supportlibs/adm_adapter/src/audio_interface_lib_common.c",
      ]
    }
    include_dirs = [
      "$hdf_hdi_service_path/primary_impl/include",
      "$hdf_audio_path/supportlibs/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "$hdf_hdi_service_path/vendor_interface/utils",
    ]

    if (drivers_peripheral_audio_feature_alsa_lib) {
      include_dirs += [
        "//third_party/cJSON",
        "//third_party/alsa-lib/include",
        "$hdf_audio_path/supportlibs/alsa_adapter/include",
      ]
      public_deps += [ "//third_party/alsa-lib:libasound" ]
    } else {
      include_dirs += [ "$hdf_audio_path/supportlibs/adm_adapter/include" ]
    }

    defines = [ "AUDIO_HDI_SERVICE_MODE" ]

    if (enable_audio_hal_hdf_log) {
      defines += [ "AUDIO_HDF_LOG" ]
    }

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "drivers_interface_audio:audio_idl_headers",
        "hdf_core:libhdf_utils",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_shared_library("audio_render_adapter") {
    sources = [ "$hdf_hdi_service_path/primary_impl/src/audio_common.c" ]
    if (drivers_peripheral_audio_feature_alsa_lib) {
      sources += [
        "$hdf_audio_path/supportlibs/alsa_adapter/src/alsa_lib_common.c",
        "$hdf_audio_path/supportlibs/alsa_adapter/src/alsa_lib_render.c",
        "$hdf_audio_path/supportlibs/alsa_adapter/src/alsa_mixer_path.c",
        "//third_party/cJSON/cJSON.c",
      ]
    } else {
      sources += [
        "$hdf_audio_path/supportlibs/adm_adapter/src/audio_interface_lib_common.c",
        "$hdf_audio_path/supportlibs/adm_adapter/src/audio_interface_lib_render.c",
      ]
    }

    include_dirs = [
      "$hdf_hdi_service_path/primary_impl/include",
      "$hdf_audio_path/supportlibs/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "$hdf_hdi_service_path/vendor_interface/utils",
    ]

    if (drivers_peripheral_audio_feature_alsa_lib) {
      include_dirs += [
        "//third_party/cJSON",
        "//third_party/alsa-lib/include",
        "$hdf_audio_path/supportlibs/alsa_adapter/include",
      ]
      public_deps += [ "//third_party/alsa-lib:libasound" ]
    } else {
      include_dirs += [ "$hdf_audio_path/supportlibs/adm_adapter/include" ]
    }

    defines = [ "AUDIO_HDI_SERVICE_MODE" ]

    if (enable_audio_hal_hdf_log) {
      defines += [ "AUDIO_HDF_LOG" ]
    }

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "drivers_interface_audio:audio_idl_headers",
        "hdf_core:libhdf_utils",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }
}
